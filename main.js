import Circle from './classes/Circle.js';
import Square from './classes/Square.js';

const app = new PIXI.Application({
	width: 800,
	height: 600,
	backgroundColor: 0x1099bb,
	resolution: window.devicePixelRatio || 1,
});
document.body.appendChild(app.view);

const container1 = new PIXI.Container();
const container2 = new PIXI.Container();

const levels = [
	[
		{ x: 2, y: 1, type: 'line' },
		{ x: 3, y: 1, type: 'line' },
		{ x: 1, y: 3, type: 'point' },
		{ x: 2, y: 4, type: 'point' },
		{ x: 3, y: 4, type: 'point' },
		{ x: 4, y: 3, type: 'point' },
	],
	[
		{ x: 1, y: 4, type: 'point' },
		{ x: 2, y: 4, type: 'point' },
		{ x: 3, y: 4, type: 'point' },
		{ x: 4, y: 4, type: 'point' },
		{ x: 5, y: 4, type: 'point' },
		{ x: 2, y: 1, type: 'line' },
		{ x: 4, y: 1, type: 'line' },
	],
]

const LevelsCellCount = {
	0: 5,
	1: 6,
}

const CELL_SIZE = 35;
let CORRECT_ITEMS = 0;
let CURRENT_LEVEL = 1;

const texture = PIXI.Texture.from('cell.png');

printGrid(texture, container1);
printGrid(texture, container2);

printLevel(container2, levels[CURRENT_LEVEL]);

container1.x = app.screen.width / 10;
container1.y = app.screen.height / 5;

container2.x = 6 * app.screen.width / 10;
container2.y = app.screen.height / 5;

app.stage.addChild(container1);
app.stage.addChild(container2);

const mainPoint = new Circle(7 * app.screen.width / 10, app.screen.height - 100);
mainPoint.print();
app.stage.addChild(mainPoint);

const mainLine = new Square(8 * app.screen.width / 10, app.screen.height - 100);
mainLine.print();
app.stage.addChild(mainLine);

mainPoint.interactive = true;
mainPoint.buttonMode = true;
mainLine.interactive = true;
mainLine.buttonMode = true;

mainPoint
	.on('mousedown', onDragStart)
	.on('touchstart', onDragStart)
	.on('mouseup', onDragEnd)
	.on('mouseupoutside', onDragEnd)
	.on('touchend', onDragEnd)
	.on('touchendoutside', onDragEnd)
	.on('mousemove', onDragMove)
	.on('touchmove', onDragMove);

mainLine
	.on('mousedown', onDragStart)
	.on('touchstart', onDragStart)
	.on('mouseup', onDragEnd)
	.on('mouseupoutside', onDragEnd)
	.on('touchend', onDragEnd)
	.on('touchendoutside', onDragEnd)
	.on('mousemove', onDragMove)
	.on('touchmove', onDragMove);

function onDragEnd() {
	let containerPos = {
		x: Math.floor(this.x - container1.x),
		y: Math.floor(this.y - container1.y)
	}
	let item;
	for (let i = 0; i < levels[CURRENT_LEVEL].length; i++) {
		if (disance(containerPos, levels[CURRENT_LEVEL][i]) < 10) {
			if (levels[CURRENT_LEVEL][i].type === 'point' && this instanceof Circle) {
				item = new Circle(levels[CURRENT_LEVEL][i].x * CELL_SIZE, levels[CURRENT_LEVEL][i].y * CELL_SIZE);
				item.print();
				container1.addChild(item);
				CORRECT_ITEMS++;
			}
			if (levels[CURRENT_LEVEL][i].type === 'line' && this instanceof Square) {
				item = new Square(levels[CURRENT_LEVEL][i].x * CELL_SIZE, levels[CURRENT_LEVEL][i].y * CELL_SIZE);
				item.print();
				item.pivot.x = item.width / 2;
				container1.addChild(item);
				CORRECT_ITEMS++;
			}
		}
		this.x = this.posX;
		this.y = this.posY;
	};

	this.alpha = 1;
	this.dragging = false;
	this.data = null;
	checkWin();
}

function checkWin() {
	if (CORRECT_ITEMS === levels[CURRENT_LEVEL].length) {
		CURRENT_LEVEL++;
		alert(`You win level ${CURRENT_LEVEL}!`);
		winLevel()
		if (CURRENT_LEVEL > levels.length - 1) {
			alert("You win the Game!");
			CURRENT_LEVEL = 0;
			printGrid(texture, container1);
			printGrid(texture, container2);
			printLevel(container2, levels[CURRENT_LEVEL]);
		}
		else {
			printGrid(texture, container1);
			printGrid(texture, container2);
			printLevel(container2, levels[CURRENT_LEVEL]);
		}
	}
}

function winLevel() {
	CORRECT_ITEMS = 0;
	clearLevel(container1);
	clearLevel(container2);
}

function printGrid(texture, containerName) {
	let cellInRow = LevelsCellCount[CURRENT_LEVEL];
	let size = cellInRow * cellInRow;

	for (let i = 0; i < size; i++) {
		let bunny = new PIXI.Sprite(texture);
		bunny.x = (i % cellInRow) * CELL_SIZE;
		bunny.y = Math.floor(i / cellInRow) * CELL_SIZE;
		containerName.addChild(bunny);
	}
}

function printLevel(container, level) {
	let item;
	level.forEach(elem => {
		switch (elem.type) {
			case 'point':
				item = new Circle(elem.x * CELL_SIZE, elem.y * CELL_SIZE);
				item.print();
				break;
			case 'line':
				item = new Square(elem.x * CELL_SIZE, elem.y * CELL_SIZE);
				item.print();
				item.pivot.x = item.width / 2;
				break;
		}
		container.addChild(item);
	})
}

function clearLevel(containerName) {
	for (let i = containerName.children.length - 1; i >= 0; i--)
		containerName.removeChild(containerName.children[i]);
}

function disance(point1, point2) {
	return Math.sqrt((point1.x - point2.x * CELL_SIZE) * (point1.x - point2.x * CELL_SIZE)
		+ (point2.y * CELL_SIZE - point1.y) * (point2.y * CELL_SIZE - point1.y));
}

function onDragMove() {
	if (this.dragging) {
		var newPosition = this.data.getLocalPosition(this.parent);
		this.position.x = newPosition.x;
		this.position.y = newPosition.y;
	}
}

function onDragStart(event) {
	this.data = event.data;
	this.alpha = 0.5;
	this.dragging = true;
}