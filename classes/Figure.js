export default class Figure extends PIXI.Graphics {
    constructor(x, y) {
        super();
        this.x = x;
        this.y = y;
        this.posX = x;
        this.posY = y;
    }
}
