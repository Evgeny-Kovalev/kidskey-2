import Figure from './Figure.js'

export default class Circle extends Figure {
    constructor(x, y) {
        super(x, y);
    }

    print() {
        this.lineStyle(0);
        this.beginFill(0x000000, 1, 1);
        this.drawCircle(0, 0, 6);
        this.endFill();
    }
}