import Figure from './Figure.js'

export default class Square extends Figure {
    constructor(x, y) {
        super(x, y);
    }

    print() {
        this.lineStyle(0);
        this.beginFill(0x000000, 1, 1);
        this.drawRect(0, 0, 6, 35);
        this.endFill();
    }
}